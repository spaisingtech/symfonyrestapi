<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use DataTrans\Entities\WebEDI\Service\Partners;

/**
 * Description of PartnerManager
 *
 * @author vikrant
 */
class PartnerManager {

    private $partners;

    public function __construct(Partners $partners) {
        $this->partners = $partners;
    }

    public function getAll($userId,$limit,$page) {
        return $this->partners->getList($userId,$limit,$page);
    }

    public function showResponse($userId,$Id) {
        return $this->partners->show($userId,$Id);
    }

    public function integrate($userId,$erpid) {
        return $this->partners->integrate($userId,$erpid);
    }

}
