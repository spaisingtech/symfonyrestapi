<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use DataTrans\Entities\WebEDI\Service\Inventory;
use DataTrans\Entities\WebEDI\Service\Messages;

/**
 * Description of PartnerManager
 *
 * @author vikrant
 */
class MessageManager
{

    private $messages;

    function __construct(Messages $messages)
    {
        $this->messages = $messages;
    }

    public function getAll($userId,$download,$sdate,$edate,$tp,$docType,$limit,$page)
    {
        return $this->messages->getAll($userId,$download,$sdate,$edate,$tp,$docType,$limit,$page);
    }

}
