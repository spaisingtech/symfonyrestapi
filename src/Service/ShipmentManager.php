<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use DataTrans\Entities\WebEDI\Service\Shipments;

/**
 * Description of PartnerManager
 *
 * @author vikrant
 */
class ShipmentManager {

    private $shipments;

    public function __construct(Shipments $shipments) {
        $this->shipments = $shipments;
    }

    public function getAll($userId,$download,$sdate,$edate,$tp, $limit, $page) {
      //  die('ShipmentManager');
        return $this->shipments->getAll($userId,$download,$sdate,$edate,$tp, $limit, $page);
    }



}
