<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use DataTrans\Entities\WebEDI\Service\Messages;
use DataTrans\Entities\WebEDI\Service\Invoices;
use DataTrans\Entities\WebEDI\Service\Documents;
use DataTrans\Entities\WebEDI\Service\DataTree;
use DataTrans\Entities\WebEDI\Service\Nodes;
use App\Helper\EDI;

/**
 * Description of PartnerManager
 *
 * @author vikrantx`
 */
class InvoiceManager {

    private $inovice;
    private $messages;
    private $documents;
    private $dataTree;
    private $nodes;

    public function __construct(Invoices $inovice, Messages $messages, Documents $documents, DataTree $dataTree, Nodes $nodes) {
        $this->inovice = $inovice;
        $this->messages = $messages;
        $this->documents = $documents;
        $this->nodes = $nodes;
        $this->dataTree = $dataTree;
    }

    public function getAll($userId, $download, $sdate, $edate, $tp, $limit, $page) {
        return $this->inovice->getAll($userId, $download, $sdate, $edate, $tp, $limit, $page);
    }

    public function getMsgDetails($MsgId, $userId) {
        return $this->inovice->getMsgDetails($MsgId, $userId);
    }

    public function createInvoice($userId, $payload,$tp,$doctype) {
        $doctype = $doctype; //considering fix
        $tp = $tp;
        $edi = new EDI($payload);
        $ediarry = $edi->parse();
        if ($ediarry === false) {
            throw new \Execption("Invalid data.");
        }
        return $this->uploadEDIAsMessage($userId, $ediarry,$tp,$doctype);
        //print_r(json_decode($payload, true)); exit;
    }

    public function uploadEDIAsMessage($userId, $ediarry, $tp, $doctype) {
        // $doctype = 860; //considering fix
        // $tp = 3299;   //21 BBB  #2825 Cambridge 810   #sonoma 855 3299 #846 PETCO 609    #860 sonoma 3299

        $doctype = $doctype; //considering fix
        $tp = $tp;   //21 BBB  #2825 Cambridge 810   #sonoma 855 3299 #846 PETCO 609    #860 sonoma 3299

        $response = array();
        foreach ($ediarry as $edi) {
            $domInfo = $this->documents->getDom($userId, $tp, $doctype);
            //print_r($domInfo); exit;
            if (!$domInfo) {
                throw new \Exception("Sorry could not find Document Object Model for this trading partner. Please contact DataTrans Solutions.");
            }

            $version = $domInfo["version"];
            $domroot = $domInfo["rootnode"];
            $dom = $this->nodes->load($domroot);

            $total = 0;
            $arr_calendar_events = array();
            $tree = $this->nodes->load(null);
            foreach ($edi as $segment) {
                $node = $this->nodes->load(null);
                $node->setTitle($segment[0]);
                $hlType = ($segment[0] == 'HL') ? $segment[3] : null;
                $addressType = ($segment[0] == 'N1') ? $segment[1] : null;
                if($segment[0] == 'REF') {
                    $addressType = $segment[1];
                }

                if ($this->dataTree->insertNode($node, $tree, $dom, $hlType, $addressType)) {
                    $this->dataTree->insertElements($segment, $tree, $dom);
                } else {
                    $errormessage = "Error inserting document <BR>Document type {$doctype}.
                                    <BR>* Error inserting node {$segment[0]}.
                                    <BR>* Segment invalid or misplaced ($domroot).";
                    throw new \Exception($errormessage);
                }

                if (isset($segment[0]) && $segment[0] == 'PO1') {
                    $total += floatval($segment[2]) * floatval($segment[4]);
                } elseif (isset($segment[0]) && $segment[0] == 'DTM') {
                    switch ($segment[1]) {
                        case '079':
                        case '002':
                        case '010':
                        case '064':
                        case '068':
                        case '069':
                        case '038':
                            $shipdate = date("Y-m-d", strtotime($segment[2]));
                            break;
                    }
                    ### Calendar events ... gathering of information
                    $arr_calendar_events[$segment[1]] = $segment[2];
                }

                //print_r($node);
                //exit;
            }

            // We've inserted all the data. Now it's time to add the Messages table.

            $root = null;
            while ($tree->parent()) {
                $tree = $this->nodes->load($tree->parent());
            }
            $root = $tree->pointer();

            //$total, $shipdate //TODO: insert
            $newMsgId = $this->messages->create($userId, $tp, "", $doctype, $version, $root, "draft", "");
            $response[] =  $newMsgId;
        }
        //print($newMsgId); exit;
        return $response;
    }

}
