<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use DataTrans\Entities\WebEDI\Service\APIDocument;

/**
 * Description of PartnerManager
 *
 * @author vikrant
 */
class APIDocumentManager {

    private $document;

    public function __construct(APIDocument $document) {
        $this->document = $document;
    }

    public function getAll() {

      return $this->document->getList();
    }

    public function getAllResponse() {

      return $this->document->getResponseList();
    }




}
