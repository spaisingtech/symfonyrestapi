<?php

// //namespace App\Component\HttpFoundation;
// namespace App\Service;
// use Symfony\Component\HttpFoundation\Response as xmlResponse;
//
// class XmlResponse1 extends Response
// {
//     public function __construct(?string $content = '', int $status = 200, array $headers = [])
//     {
//         parent::__construct($content, $status, array_merge($headers, [
//             'Content-Type' => 'text/xml',
//         ]));
//     }
//
//     public static function create($result, $error, $code = 200, $info = null, $headers = [])
//     {
//
//       echo "xml called";
//
//         if (strlen($error) && $code == 200) {
//             $code = 400;
//         }
//
//         $body = array(
//             'result' => $result,
//             'error' => $error,
//         );
//         if ($info && is_array($info)) {
//             $body['info'] = $info;
//         }
//         return new xmlResponse($body, $code, $headers);
//     }
//
//
// }


namespace App\Service;

use DOMDocument;


class ConverterHl7
{
    public function convert(){

        $dom = new DOMDocument('1.0', 'utf-8');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;

        $clinical_document = $dom->createElement('ClinicalDocument');
        $realm_code =  $dom->createElement('realmCode');

        $realm_code->setAttribute( "CODE:", "IT" );

        $clinical_document->appendChild($realm_code);

        return $dom->saveXML();

    }

}
