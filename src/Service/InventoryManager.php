<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use DataTrans\Entities\WebEDI\Service\Inventory;

/**
 * Description of PartnerManager
 *
 * @author vikrant
 */
class InventoryManager {

    private $shipments;

    public function __construct(Inventory $inventory) {
        $this->inventory = $inventory;
    }

    public function getAll($userId,$download,$sdate,$edate,$tp,$limit,$page) {
        //die('InvetoryManager');
        return $this->inventory->getAll($userId,$download,$sdate,$edate,$tp,$limit,$page);
    }

    public function getAllChangePO($userId,$download,$sdate,$edate,$tp) {
        //die('InvetoryManager');
        return $this->inventory->getAllChangePO($userId,$download,$sdate,$edate,$tp);
    }




}
