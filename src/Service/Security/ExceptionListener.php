<?php
namespace App\Service\Security;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getThrowable();
          $request   = $event->getRequest();
          $routeName = $request->get('_route');

          //die($routeName);
          if (!$event->isMainRequest()) {
              return;
          }

          if (in_array(
              $routeName, array('default','login','document')
          )) {
              return;
          }
        //print_r($exception);

        // $message = sprintf(
        //     'Rest API Error says: %s with code: %s status code %s',
        //     $exception->getMessage(),
        //     $exception->getCode(),
        //     $exception->getStatusCode()
        // );

        $message = sprintf(
            'Rest API Error says: %s with code: %s' ,
            $exception->getMessage(),
            $exception->getCode()

        );


        // if($exception->getStatusCode() == '404')
        // {
        //   $message = sprintf(
        //       'Rest API Error says: Please check the URL'
        //   );
        // }
        //
        // if($exception->getStatusCode() == '405')
        // {
        //   $message = sprintf(
        //       'Rest API Error says: Please check the Method'
        //   );
        // }
        //
        // if($exception->getStatusCode() == '500')
        // {
        //   $message = sprintf(
        //       'Rest API Error says: Internal Server Error'
        //   );
        // }



        // Customize your response object to display the exception details
        $response = new Response();
        $response->setContent($message);

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        // sends the modified response object to the event
        $event->setResponse($response);
    }
}
?>
