<?php

namespace App\Service\Security;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//use App\Service\JSONResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Util;

/**
 * @codeCoverageIgnore
 */
class AuthListener {

    private $sm;
    private $router;
    private $jwtSecretKey;

    public function __construct(/**SessionManager $sm, */ $jwtSecretKey, UrlGeneratorInterface $router) {
        //$this->sm = $sm;
        $this->jwtSecretKey = $jwtSecretKey;
        $this->router = $router;

    }

    public function onKernelRequest(RequestEvent $event) {
        $kernel    = $event->getKernel();
        $request   = $event->getRequest();
        $routeName = $request->get('_route');
        $path = $request->getRequestUri();

        //die($routeName);


        if (!$event->isMainRequest()) {
            return;
        }

        if (in_array(
            $routeName, array('default','login','document','document1')
        )) {
            return;
        }

        $token = $request->headers->get('x-auth-token');
        if(!$token) {
            $response = new Response('Access denied. No token provided.', Response::HTTP_UNAUTHORIZED);
            $event->setResponse($response);
        } else {
            list($signatureValid, $payload)  = Util::isTokenValid($token, $this->jwtSecretKey);
            if(!$signatureValid) {
                $response = new Response('Invalid token.', Response::HTTP_BAD_REQUEST);
                $event->setResponse($response);
            } else {
                $payload = json_decode($payload, true);
                $request->request->set('userId', $payload['user_id']);
            }
        }
    }

}
