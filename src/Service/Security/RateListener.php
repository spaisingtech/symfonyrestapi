<?php

namespace App\Service\Security;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//use App\Service\JSONResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Util;


use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\RateLimiter\RateLimiterFactory;


/**
 * @codeCoverageIgnore
 */
class RateListener {

    private $sm;
    private $router;
    private $anonymousApiLimiter;


    public function __construct( UrlGeneratorInterface $router, RateLimiterFactory $anonymousApiLimiter) {
        //$this->sm = $sm;

        $this->router = $router;
        $this->anonymousApiLimiter = $anonymousApiLimiter;

    }

    public function onKernelRequest(RequestEvent $event ) {

        $kernel    = $event->getKernel();
        $request   = $event->getRequest();
        $routeName = $request->get('_route');
        $path = $request->getRequestUri();

        // Check master request
        if (!$event->isMainRequest()) {
            return;
        }

        if (in_array(
            $routeName, array('default','login')
        )) {
            return;
        }
        //$query_string=$request->getQueryString();

        $token = $request->headers->get('x-auth-token');

        //die('API rate Limiter');

        $limiter = $this->anonymousApiLimiter->create($request->getClientIp());
        if (false === $limiter->consume(1)->isAccepted()) {
            throw new TooManyRequestsHttpException();
        }
    }

}
