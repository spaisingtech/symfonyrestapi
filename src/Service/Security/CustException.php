<?php

namespace App\Service\Security;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\Cookie;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Util;


use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\RateLimiter\RateLimiterFactory;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;


/**
 * @codeCoverageIgnore
 */
class CustException {

    private $sm;
    private $router;
    private $anonymousApiLimiter;


    public function __construct( UrlGeneratorInterface $router, RateLimiterFactory $anonymousApiLimiter) {
        //$this->sm = $sm;

        $this->router = $router;
        $this->anonymousApiLimiter = $anonymousApiLimiter;

    }


    public function onKernelException(RequestEvent $event): void
    {
        $exception = $event->getException();

        if ($exception instanceof NotFoundHttpException) {
            $response = $this->resourceNotFoundResponse(json_encode($exception->getMessage()));
        }

        if (isset($response)) {
            $event->setResponse($response);
        }
    }


}
