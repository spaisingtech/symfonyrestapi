<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\JsonResponse as JResponse;;

class OutPutResponse {

    public static function create($result, $type='json', $error, $code = 200, $info = null, $headers = [])
    {

      if($type == 'json')
      {
            if (strlen($error) && $code == 200)
            {
                $code = 400;
            }

            $body = array(
                'result' => $result,
                'error' => $error,
            );
            if ($info && is_array($info)) {
                $body['info'] = $info;
            }
            return new Response($body, $code, $headers);

      }
    }
}
