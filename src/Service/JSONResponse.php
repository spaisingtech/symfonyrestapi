<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\JsonResponse as JResponse;

class JSONResponse {

    public static function create($result, $error, $code = 200, $info = null, $headers = [])
    {

        if (strlen($error) && $code == 200) {
            $code = 400;
        }

        $body = array(
            'result' => $result,
            'error' => $error,
        );
        if ($info && is_array($info)) {
            $body['info'] = $info;
        }
        return new JResponse($body, $code, $headers);
    }

}
