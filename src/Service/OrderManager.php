<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use DataTrans\Entities\WebEDI\Service\Orders;
//use DataTrans\Entities\WebEDI\Service\Messages;
/**
 * Description of PartnerManager
 *
 * @author vikrantx`
 */
class OrderManager {

    private $orders;
    //private $msg;

    public function __construct(Orders $orders) {
      $this->orders = $orders;
      //$this->msg = $msg;
    }

    public function getAll($userId,$download,$sdate,$edate,$tp,$limit,$page)
    {
         return $this->orders->getAll($userId,$download,$sdate,$edate,$tp,$limit,$page);
    }

    public function getAllAkw($userId,$download,$sdate,$edate,$tp,$limit,$page)
    {
         return $this->orders->getAllAkw($userId,$download,$sdate,$edate,$tp,$limit,$page);
    }

    public function getMsgDetails($MsgId,$userId)
    {
          return $this->orders->getMsgDetailsNew($MsgId,$userId);
    }

    public function orderMark($MsgId,$userId,$mark,$correlative)
    {
          return $this->orders->mark($MsgId,$userId,$mark,$correlative);
    }

    public function bulkShow($userId,$download,$sdate,$edate,$tp,$limit,$page)
    {
          return $this->orders->bulkShow($userId,$download,$sdate,$edate,$tp,$limit,$page);
    }


}
