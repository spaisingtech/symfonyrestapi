<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use DataTrans\Entities\WebEDI\Service\Catalogs;

/**
 * Description of PartnerManager
 *
 * @author vikrant
 */
class CatalogManager {

    private $catalogs;

    public function __construct(Catalogs $catalogs) {
        $this->catalogs = $catalogs;
    }

    public function getAll($userId,$limit,$page) {
        return $this->catalogs->getList($userId,$limit,$page);
    }

    public function getPerticular($userId,$Id) {
        return $this->catalogs->show($userId,$Id);
    }

    public function integrate($userId,$prodyctID,$erpid) {
        return $this->catalogs->integrate($userId,$prodyctID,$erpid);
    }

}
