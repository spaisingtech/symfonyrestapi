<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helper;

/**
 * Description of EDI
 *
 * @author vikrant
 */
class EDI {

    var $data;
    var $err;

    function __construct($data, $err = "") {
        $this->data = $data;
        $this->err = $err;
    }

    // phpcs:disable
    function parse() {
        $ediArray = array();
        $data = $this->data;

        if (trim($data) == "") {
            $this->err = "No data to parse.";
            return false;
        }
        
        if(!is_array($data)) {
            $data = json_decode($data, true);
            if(json_last_error() !== JSON_ERROR_NONE) {
                $this->err = "Invalid payload.";
                return false;
            }
        }
        
        foreach($data as $dockey => $document) {
            //print_r($document);
            $docArray = array();
            foreach($document as $segkey => $segment) {
                $segsArray = array();
                $segArray = $this->parseSegment($segment, $segsArray);
                //if($segment['title'] == "N1") {
                //    print_r($segsArray); exit;
                //}
                //$docArray[] = $segArray;
                $docArray = array_merge($docArray, $segsArray);
            }
            $ediArray[] = $docArray;
            //print_r($docArray); exit;
        } 
        
        return $ediArray;
    }
    
    function parseSegment($segment, &$segsArray) {
        $segmentTitle = $segment['title'];
        $elements = $segment['elements']['node'];
        $segArray = array();
        $segArray[0] = $segmentTitle;
        foreach($elements as $elementIndex => $element) {
            //echo $element['index']; exit;
            $index = intval($element["index"]);
            $segArray[$index] = $element['data'];
        }
        $segsArray[] = $segArray;
        if(isset($segment['children'])) {
            foreach($segment['children']['node'] as $childsegment) {
                $this->parseSegment($childsegment, $segsArray);
            }
        }
        return $segsArray;
    }

}
