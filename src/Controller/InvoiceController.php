<?php

namespace App\Controller;

ini_set('max_execution_time', '0');

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Service\InvoiceManager;

class InvoiceController extends AppController {

    private $invoiceManager;

    public function __construct(InvoiceManager $invoiceManager) {
        $this->invoiceManager = $invoiceManager;
    }

    public function getAll(Request $request): Response {
        try {

            $userId = $request->request->get('userId');
            $outputFormat = $request->get('outputformat');
            $reqBody = $request->getContent();
            $data = $this->requestData($request);

            if (!array_key_exists("download", $data)) {
                return $this->jsonResponse(null, "required  download", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("startDate", $data)) {
                return $this->jsonResponse(null, "required  start date", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("endDate", $data)) {
                return $this->jsonResponse(null, "required  end date", Response::HTTP_FORBIDDEN);
            }

            $download = $data['download'];
            $sdate = $data['startDate'] ?? '';
            $edate = $data['endDate'] ?? '';
            $tp = '';
            $page = 1;
            $limit = 100;

            if (array_key_exists("tp", $data)) {
                $tp = $data['tp'];
            }

            if (array_key_exists("limit", $data) && !empty($data['limit'])) {
                $limit = $data['limit'];
            }

            if (array_key_exists("page", $data) && !empty($data['page'])) {
                $page = $data['page'];
            }

            if ($download == '') {
                return $this->jsonResponse(null, "download can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($sdate == '') {
                return $this->jsonResponse(null, "start date can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($edate == '') {
                return $this->jsonResponse(null, "end date can not be empty", Response::HTTP_FORBIDDEN);
            }

            if ($edate < $sdate) {
                return $this->jsonResponse(null, "start date can not be greater that end date", Response::HTTP_FORBIDDEN);
            }

            $list = $this->invoiceManager->getAll($userId, $download, $sdate, $edate, $tp, $limit, $page);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getMsgDetails(Request $request): Response {
        try {
            $requestData = $this->requestData($request);
            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];
            $MsgId = $requestData['id'];

            $list = $this->invoiceManager->getMsgDetails($MsgId, $userId);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function create(Request $request) {
        try {
            $requestData = $this->requestData($request);
            $payload = $requestData['payload'];

            if (!array_key_exists("tp", $requestData)) {
                return $this->jsonResponse(null, "required  tp", Response::HTTP_FORBIDDEN);
            }

            if (!array_key_exists("doctype", $requestData)) {
                return $this->jsonResponse(null, "required  doctype", Response::HTTP_FORBIDDEN);
            }

            $tp = $requestData['tp'];
            $doctype = $requestData['doctype'];

            if ($payload == '') {
                return $this->jsonResponse(null, "payload can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($tp == '') {
                return $this->jsonResponse(null, "tp can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($doctype == '') {
                return $this->jsonResponse(null, "doc type can not be empty", Response::HTTP_FORBIDDEN);
            }

            $response = $this->invoiceManager->createInvoice($requestData['userId'], $requestData['payload'], $tp, $doctype);
            return $this->jsonResponse($response);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
