<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Service\OrderManager;

class OrderController extends AppController {

    private $orderManager;

    public function __construct(OrderManager $orderManager) {
        $this->orderManager = $orderManager;
    }

    public function getAll(Request $request): Response {
        try {
            $requestData = $this->requestData($request);

            if (!array_key_exists("download", $requestData)) {
                return $this->jsonResponse(null, "required  download", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("startDate", $requestData)) {
                return $this->jsonResponse(null, "required  start date", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("endDate", $requestData)) {
                return $this->jsonResponse(null, "required  end date", Response::HTTP_FORBIDDEN);
            }

            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];
            $download = $requestData['download'];
            $sdate = $requestData['startDate'];
            $edate = $requestData['endDate'];
            $tp = '';
            $limit = 1;
            $limit = 100;

            if (array_key_exists("tp", $requestData) && !empty($requestData['tp'])) {
                $tp = $requestData['tp'];
            }

            if (array_key_exists("limit", $requestData) && !empty($requestData['limit'])) {
                $limit = $requestData['limit'];
            }
            if (array_key_exists("page", $requestData) && !empty($requestData['page'])) {
                $page = $requestData['page'];
            }

            if (empty($download) && $download != 0) {
                return $this->jsonResponse(null, "download can not be empty", Response::HTTP_FORBIDDEN);
            }
            if (empty($sdate && $sdate != 0)) {
                return $this->jsonResponse(null, "start date can not be empty", Response::HTTP_FORBIDDEN);
            }
            if (empty($edate && $edate != 0)) {
                return $this->jsonResponse(null, "end date can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($edate < $sdate) {
                return $this->jsonResponse(null, "start date can not be greater that end date", Response::HTTP_FORBIDDEN);
            }

            $list = $this->orderManager->getAll($userId, $download, $sdate, $edate, $tp, $limit, $page);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAllAkw(Request $request): Response {
        try {
            $requestData = $this->requestData($request);

            if (!array_key_exists("download", $requestData)) {
                return $this->jsonResponse(null, "required  download", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("startDate", $requestData)) {
                return $this->jsonResponse(null, "required  start date", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("endDate", $requestData)) {
                return $this->jsonResponse(null, "required  end date", Response::HTTP_FORBIDDEN);
            }

            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];
            $download = $requestData['download'];
            $sdate = $requestData['startDate'];
            $edate = $requestData['endDate'];
            $tp = '';
            $page = 1;
            $limit = 100;

            if (array_key_exists("tp", $requestData) && !empty($requestData['tp'])) {
                $tp = $requestData['tp'];
            }

            if (array_key_exists("limit", $requestData) && !empty($requestData['limit'])) {
                $limit = $requestData['limit'];
            }

            if (array_key_exists("page", $requestData) && !empty($requestData['page'])) {
                $page = $requestData['page'];
            }

            if ($download == '') {
                return $this->jsonResponse(null, "download can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($sdate == '') {
                return $this->jsonResponse(null, "start date can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($edate == '') {
                return $this->jsonResponse(null, "end date can not be empty", Response::HTTP_FORBIDDEN);
            }

            if ($edate < $sdate) {
                return $this->jsonResponse(null, "start date can not be greater that end date", Response::HTTP_FORBIDDEN);
            }

            $list = $this->orderManager->getAllAkw($userId, $download, $sdate, $edate, $tp, $limit, $page);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getMsgDetails(Request $request): Response {
        try {
            $requestData = $this->requestData($request);
            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];
            $MsgId = $requestData['id'];
            $list = $this->orderManager->getMsgDetails($MsgId, $userId);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function markOrder(Request $request): Response {
        try {
            $requestData = $this->requestData($request);
            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];
            $MsgId = $requestData['id'];

            if (!array_key_exists("mark", $requestData)) {
                return $this->jsonResponse(null, "required  mark", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("correlative", $requestData)) {
                return $this->jsonResponse(null, "required  correlative", Response::HTTP_FORBIDDEN);
            }

            $mark = $requestData['mark'];
            $correlative = $requestData['correlative'];
            if ($mark == '') {
                return $this->jsonResponse(null, "mark can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($correlative == '') {
                return $this->jsonResponse(null, "correlative can not be empty", Response::HTTP_FORBIDDEN);
            }

            $list = $this->orderManager->orderMark($MsgId, $userId, $mark, $correlative);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function bulkShow(Request $request): Response {
        try {
            $requestData = $this->requestData($request);

            if (!array_key_exists("download", $requestData)) {
                return $this->jsonResponse(null, "required  download", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("startDate", $requestData)) {
                return $this->jsonResponse(null, "required  start date", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("endDate", $requestData)) {
                return $this->jsonResponse(null, "required  end date", Response::HTTP_FORBIDDEN);
            }

            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];
            $download = $requestData['download'];
            $sdate = $requestData['startDate'];
            $edate = $requestData['endDate'];
            $tp = '';
            $page = 1;
            $limit = 100;

            if (array_key_exists("tp", $requestData) && !empty($requestData['tp'])) {
                $tp = $requestData['tp'];
            }

            if (array_key_exists("limit", $requestData) && !empty($requestData['limit'])) {
                $limit = $requestData['limit'];
            }

            if (array_key_exists("page", $requestData) && !empty($requestData['page'])) {
                $page = $requestData['page'];
            }


            if ($download == '') {
                return $this->jsonResponse(null, "download can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($sdate == '') {
                return $this->jsonResponse(null, "start date can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($edate == '') {
                return $this->jsonResponse(null, "end date can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($edate < $sdate) {
                return $this->jsonResponse(null, "start date can not be greater that end date", Response::HTTP_FORBIDDEN);
            }


            $list = $this->orderManager->bulkShow($userId, $download, $sdate, $edate, $tp, $limit, $page);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}

?>
