<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Service\MessageManager;

class MessageController extends AppController {

    private $messageManager;

    public function __construct(MessageManager $messageManager) {
        $this->messageManager = $messageManager;
    }

    public function getAll(Request $request): Response {
        try {
            $requestData = $this->requestData($request);
            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];

            $download = $requestData['download'] ?? '';
            $sdate = $requestData['startDate'] ?? '';
            $edate = $requestData['endDate'] ?? '';
            $tp = '';
            $page = 1;
            $limit = 100;

            if (array_key_exists("tp", $requestData)) {
                $tp = $requestData['tp'];
            }

            if (array_key_exists("limit", $requestData) && !empty($requestData['limit'])) {
                $limit = $requestData['limit'];
            }
            if (array_key_exists("page", $requestData) && !empty($requestData['page'])) {
                $page = $requestData['page'];
            }

            $docType = $requestData['docType'] ?? '';

            if (!array_key_exists("download", $requestData)) {
                return $this->jsonResponse(null, "required  download", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("startDate", $requestData)) {
                return $this->jsonResponse(null, "required  start date", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("endDate", $requestData)) {
                return $this->jsonResponse(null, "required  end date", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("docType", $requestData)) {
                return $this->jsonResponse(null, "required  document type", Response::HTTP_FORBIDDEN);
            }


            if ($download == '') {
                return $this->jsonResponse(null, "download can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($sdate == '') {
                return $this->jsonResponse(null, "start date can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($edate == '') {
                return $this->jsonResponse(null, "end date can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($docType == '') {
                return $this->jsonResponse(null, "docType can not be empty", Response::HTTP_FORBIDDEN);
            }

            if ($edate < $sdate) {
                return $this->jsonResponse(null, "start date can not be greater that end date", Response::HTTP_FORBIDDEN);
            }

            $list = $this->messageManager->getAll($userId, $download, $sdate, $edate, $tp, $docType, $limit, $page);

            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}

?>
