<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as Controller;
use App\Service\JSONResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse as JResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * @codeCoverageIgnore
 */
abstract class AppController extends Controller {

    protected $xml;
    public $xslt = null;
    protected $buffer;
    protected $line_delimiter;
    protected $field_delimiter;
    protected $field_delimiters;
    protected $field_enclosure;

    function __construct() {
        $this->line_delimiter = "\r\n";
        $this->buffer = "";
        $this->field_delimiters = array("\n", ',', ':', ';', '|', '@', '#', '$', '%', '^', '*');
        $this->field_enclosure = "\"";
    }

    /**
     * @var integer HTTP status code - 200 (OK) by default
     */
    protected $statusCode = 200;

    /**
     * Gets the value of statusCode.
     *
     * @return integer
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * Sets the value of statusCode.
     *
     * @param integer $statusCode the status code
     *
     * @return self
     */
    protected function setStatusCode($statusCode) {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Returns a JSON response
     *
     * @param array $data
     * @param array $headers
     *
     * @return JsonResponse
     */
    public function response($result, $format = 'json', $error = '', $code = 200, $headers = []) {
        if(strtolower($format) === 'xml') {
            return $this->xmlResponse($result);
        } else if(strtolower($format) === 'csv') {
            return $this->csvResponse($result);
        }
        return $this->jsonResponse($result, $error, $code = 200);
    }

    /**
     * Sets an error message and returns a JSON response
     *
     * @param string $errors
     * @param $headers
     * @return JsonResponse
     */
    public function respondWithErrors($errors, $headers = []) {
        $data = [
            'status' => $this->getStatusCode(),
            'errors' => $errors,
        ];

        return new JsonResponse($data, $this->getStatusCode(), $headers);
    }

    /**
     * Sets an error message and returns a JSON response
     *
     * @param string $success
     * @param $headers
     * @return JsonResponse
     */
    public function respondWithSuccess($success, $headers = []) {
        $data = [
            'status' => $this->getStatusCode(),
            'success' => $success,
        ];

        return new JsonResponse($data, $this->getStatusCode(), $headers);
    }

    /**
     * Returns a 401 Unauthorized http response
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondUnauthorized($message = 'Not authorized!') {
        return $this->setStatusCode(401)->respondWithErrors($message);
    }

    /**
     * Returns a 422 Unprocessable Entity
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondValidationError($message = 'Validation errors') {
        return $this->setStatusCode(422)->respondWithErrors($message);
    }

    /**
     * Returns a 404 Not Found
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function respondNotFound($message = 'Not found!') {
        return $this->setStatusCode(404)->respondWithErrors($message);
    }

    /**
     * Returns a 201 Created
     *
     * @param array $data
     *
     * @return JsonResponse
     */
    public function respondCreated($data = []) {
        return $this->setStatusCode(201)->response($data);
    }

    // this method allows us to accept JSON payloads in POST requests
    // since Symfony 4 doesn’t handle that automatically:

    protected function transformJsonBody(\Symfony\Component\HttpFoundation\Request $request) {
        $data = json_decode($request->getContent(), true);
        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }

    /**
     * Generate a json response for a given result and/or error.
     */
    protected final function jsonResponse($result, $error = '', $code = 200, $headers = []) {
        $info = array('originalRequest' => $this->get('request_stack')->getCurrentRequest()->getPathInfo());

        if (strlen($error) && $code == 200)
        {
            $code = 400;
        }

        $body = array(
            'result' => $result,
            'error' => $error,
        );
        if ($info && is_array($info)) {
            $body['info'] = $info;
        }
        return new JResponse($body, $code, $headers);

    }

    function csvResponse(array $fields, $delimiter = ',', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false) {


      // $delimiter_esc = preg_quote($delimiter, '/');
      // $enclosure_esc = preg_quote($enclosure, '/');

        // echo "<pre>";
        // print_r($fields);
       //exit;

        $arrrkey = '';
        $arrrVal = '';


        array_walk_recursive($fields, function ($item, $key)use(&$arrrkey,&$arrrVal)
        {


              $item=str_replace(",","?",$item);
              $arrrkey  .= $key.",";
              $arrrVal  .= $item.",";

        });

      //  echo $arrrVal;

        $arrrkey = rtrim($arrrkey,",");
        $arrrVal = rtrim($arrrVal,",");
        $arrrkey = explode(",",$arrrkey);
        $arrrVal = explode(",",$arrrVal);

         // echo "<pre>";
         // print_r($arrrVal);
         // print_r($arrrkey);


        $firstVal = $arrrkey[0];
      // echo "====".$firstVal;
        $output = '';
        for($i=0;$i<count($arrrVal);$i++)
        {
                if($firstVal == $arrrkey[$i])
                {
                    $output .= "\r\n";
                    $output .= $arrrVal[$i].",";
                }
          else {
                $output .= $arrrVal[$i].",";
              }

        }

          $output=str_replace("?",",",$output);
        // echo $output;
        // exit;

        $response = new Response($output);
        $response->headers->set('Content-Type', 'text/text');
        return $response;
        exit;
        //
        // $delimiter_esc = preg_quote($delimiter, '/');
        // $enclosure_esc = preg_quote($enclosure, '/');
        //
        // $outputString = "";
        // $i = 0;
        // foreach ($fields as $key => $tempFields) {
        //     $output = array();
        //     foreach ($tempFields as $key => $field) {
        //         if ($field === null && $nullToMysqlNull) {
        //             $output[] = 'NULL';
        //             continue;
        //         }
        //
        //         // Enclose fields containing $delimiter, $enclosure or whitespace
        //         if ($encloseAll || preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field)) {
        //             $field = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
        //         }
        //         $output[] = $field . " ";
        //     }
        //     $outputString .= implode($delimiter, $output) . "\r\n";
        // }
        //
        //
        //
        //
        // $response = new Response($outputString);
        // $response->headers->set('Content-Type', 'text/text');
        // return $response;
    }

    function xmlResponse($object) {
//        if (!headers_sent()) {
//            header('Content-Type: text/xml');
//        }
        $this->xml = simplexml_load_string('<' . '?xml version="1.0" ?' . '><root/>');

        array_walk($object, array($this, 'addNode'), $this->xml);
        $xml = $this->xml->asXML();
        if ($this->xslt != null && $this->xslt instanceof XSLTProcessor) {
            $xml = $this->xslt->transformToXml($this->xml);
        }

        $response = new Response($xml);
        $response->headers->set('Content-Type', 'text/xml');
        return $response;

        // print $xml;
    }

    function addNode($item, $key, $parent) {
        $key = $this->properXMLTag($key);
        if (is_array($item)) {

            $node = $parent->addChild($key);
            array_walk($item, array($this, 'addNode'), $node);
            return $node;
        } else {
            return $parent->addChild($key, htmlentities($item));
        }
    }

    function properXMLTag($key) {
        if (is_numeric($key)) {
            return "node";
        } else {
            return str_replace(' ', '_', htmlspecialchars($key, 3 | 16));
        }
    }

    // protected final function requireObjectProperties($object, $requirements) {
    //     if ($field = $this->findMissingProperty($object, $requirements)) {
    //         throw new \Exception("Missing required field $field.", 4293);
    //     }
    // }
    // protected final function findMissingProperty($object, $fields) {
    //     foreach ($fields as $field) {
    //         if (!isset($object[$field])) {
    //             return $field;
    //         }
    //     }
    //     return null;
    // }

    /**
     * Returns array of request data
     *
     * @param array $data
     *
     * @return Array
     */
    public function requestData(Request $request) {
        $inputs = array();
        $route_params = $request->attributes->get('_route_params');
        $req_params = $request->request->all();
        $query_params = $request->query->all();
        $rawdata = $request->getContent();
        $rawJson = json_decode($rawdata, true);
        if(json_last_error() !== JSON_ERROR_NONE) {
            parse_str($rawdata, $rawdata);
            $inputs = is_array($rawdata) ? $rawdata : array();
        }
        
        $json = is_array($rawJson) ? $rawJson : array();

        $inputs = array_merge($inputs, $route_params);
        $inputs = array_merge($inputs, $req_params);
        $inputs = array_merge($inputs, $query_params);
        $inputs = array_merge($inputs, $json);
        $flag = '';

        $inputs['route_params'] = $route_params;
        $inputs['req_params'] = $req_params;
        $inputs['query_params'] = $query_params;
        $inputs['json_params'] = $json;
        if ($request->isMethod('post')) {
            $inputs['payload'] = $request->getContent();
        }


        //
        // foreach($inputs as $key => $val)
        // {
        //       if(empty($val))
        //       {
        //         die($key.' should not be blank');
        //       }
        // }




        // echo "<pre>";
        // echo $flag;
        // print_r($inputs);
        // die();
        return $inputs;
    }

}
