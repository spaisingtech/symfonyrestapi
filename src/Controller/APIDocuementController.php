<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use App\Service\APIDocumentManager;

class APIDocuementController extends AppController
{
  private $APIDocumentManager;
  public function __construct(APIDocumentManager $APIDocumentManager)
  {
      $this->APIDocumentManager = $APIDocumentManager;
  }


  public function getHTML(Request $request): Response
  {
      $list = $this->APIDocumentManager->getAll();
      $response = $this->APIDocumentManager->getAllResponse();
      // echo "<pre>";
      // print_r($response);
      // exit;
    //die('hiiii');
    $html = '

    header("Content-Type: text/plain");
    <!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.accordion {
background-color: #eee;
color: #444;
cursor: pointer;
padding: 18px;
width: 100%;
border: none;
text-align: left;
outline: none;
font-size: 15px;
transition: 0.4s;
margin-bottom: 6px;
}

.active, .accordion:hover {
background-color: #ccc;
}

.panel {
padding: 0 18px;
display: none;
background-color: white;
overflow: hidden;
}
</style>
</head>
<body>

<h2>API Documentation </h2>

';
foreach ($list as $value) {
    $html.='    <button class="accordion">'.$value["name"].'</button>
    <div class="panel" style="border: 2px solid #d0cdcd;margin-bottom:3px;font-family:courier, courier new, serif;">
    <table border="0">
    <tr>
        <td style="width:10%;vertical-align: top;">  Method </td>
        <td style="width:50%;"> :'.$value['method'].' </td>
    </tr>

    <tr>
        <td style="width:10%;vertical-align: top;">  Description </td>
        <td style="width:80%;"> :'.$value['description'].' </td>
    </tr>
    <tr>
        <td style="width:10%;vertical-align: top;">  Query Paramters </td>
        <td style="width:80%;"> :'.$value['query_parameer'].' </td>
    </tr>
    <tr>
        <td style="width:10%;vertical-align: top;">  Body Paramters </td>
        <td style="width:80%;"> :'.$value['body_request'].'</td>
    </tr>';

    foreach ($response as $responseVal)
    {
          if($value['PK_ID'] == $responseVal['Doc_ID'])
          {

          $html .= '  <tr>
                <td style="width:10%;vertical-align: top;">  Response '.$responseVal['ResponseType'].' </td>
                <td style="width:80%;"> :'.$responseVal['ResponseBody'].'<br></td>
            </tr>';


          }

    }

    $html.='<tr>
        <td style="width:10%;vertical-align: top;">  Output Format </td>
        <td style="width:80%;"> :'.$value['formate_type'].' </td>
    </tr>
    <tr>
        <td style="width:10%;vertical-align: top;">  API </td>
        <td style="width:80%;"> :'.$value['API'].' </td>
    </tr>

  </table>
    </div>
    ';
  }
      $html.='<script>
      var acc = document.getElementsByClassName("accordion");
      var i;

      for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
      });
      }
      </script>

      </body>
      </html>';
    // return new Response(
    //      '<html><body >Hello</body></html>'
    //  );

     return new Response(
          $html
      );

  }
    public function getAll(Request $request): Response
    {
        $list = $this->APIDocumentManager->getAll();
        //return $this->render('index.html.twig');
        // echo "<pre>";
        //
        // print_r($list);

      ?>

      <!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.accordion {
background-color: #eee;
color: #444;
cursor: pointer;
padding: 18px;
width: 100%;
border: none;
text-align: left;
outline: none;
font-size: 15px;
transition: 0.4s;
}

.active, .accordion:hover {
background-color: #ccc;
}

.panel {
padding: 0 18px;
display: none;
background-color: white;
overflow: hidden;
}
</style>
</head>
<body>

<h2>API Documentation </h2>
<?php foreach ($list as $value) { ?>

<button class="accordion"> <?php  echo $value['name']; ?> </button>
<div class="panel" style="border: 2px solid #d0cdcd;">
<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> -->
Mothod : <?php echo $value['method']; ?> <br>
Description : <?php echo $value['description']; ?> <br>
Query Paramters : <?php echo $value['query_parameer']; ?> <br>
Body Paramters : <?php echo $value['body_request']; ?> <br>
Body Response : <?php echo $value['body_response']; ?> <br>
Output Format : <?php echo $value['formate_type']; ?> <br>
API : <?php echo $value['API']; ?> <br>

</div>

<?php } ?>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
acc[i].addEventListener("click", function() {
this.classList.toggle("active");
var panel = this.nextElementSibling;
if (panel.style.display === "block") {
  panel.style.display = "none";
} else {
  panel.style.display = "block";
}
});
}
</script>

</body>
</html>


<?php
    }

    //die('asddasd');
    //return 1;

}
