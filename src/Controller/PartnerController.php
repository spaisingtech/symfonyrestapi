<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Service\PartnerManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Exception;
use Symfony\Component\Routing\Annotation\Route;

class PartnerController extends AppController {

    private $partnerManager;
    private $anonymousApiLimiter;
    private $limiter;

    public function __construct(PartnerManager $partnerManager) {
        $this->partnerManager = $partnerManager;
    }

    public function getAll(Request $request): Response {
        try {
            $requestData = $this->requestData($request);
            $userId = $requestData['userId'];
            $format = $requestData['outputformat'];
            $page = 1;
            $limit = 100;

            if (array_key_exists("limit", $requestData) && !empty($requestData['limit'])) {
                $limit = $requestData['limit'];
            }
            if (array_key_exists("page", $requestData) && !empty($requestData['page'])) {
                $page = $requestData['page'];
            }


            $list = $this->partnerManager->getAll($userId, $limit, $page);
            return $this->response($list, $format);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function show(Request $request): Response {
        try {
            $requestData = $this->requestData($request);
            $outputFormat = $requestData['outputformat'];
            $userId = $requestData['userId'];
            $Id = $requestData['Id'];

            $list = $this->partnerManager->showResponse($userId, $Id);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function integrate(Request $request): Response {
        try {
            $requestData = $this->requestData($request);
            $userId = $requestData['userId'];
            $outputformat = $requestData['outputformat'];
            if (!array_key_exists("erpid", $requestData)) {
                return $this->jsonResponse(null, "required  erpid", Response::HTTP_FORBIDDEN);
            }
            $erpid = $requestData['erpid'];

            if (empty($erpid)) {
                return $this->jsonResponse(null, "erpid can not be empty", Response::HTTP_FORBIDDEN);
            }

            $list = $this->partnerManager->integrate($userId, $erpid);
            return $this->response($list, $outputformat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
