<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use DataTrans\Entities\WebEDI\Service\Catalogs;
use DataTrans\Entities\WebEDI\Service\Oauth;
use App\Service\CatalogManager;

class CatalogController extends AppController {

    private $catalogManager;

    public function __construct(CatalogManager $catalogManager) {
        $this->catalogManager = $catalogManager;
    }

    public function getAll(Request $request): Response {
        try {

            $requestData = $this->requestData($request);
            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];
            $page = 1;
            $limit = 100;

            if (array_key_exists("limit", $requestData) && !empty($requestData['limit'])) {
                $limit = $requestData['limit'];
            }
            if (array_key_exists("page", $requestData) && !empty($requestData['page'])) {
                $page = $requestData['page'];
            }

            $list = $this->catalogManager->getAll($userId, $limit, $page);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getOne(Request $request): Response {
        try {
            $requestData = $this->requestData($request);
            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];
            $Id = $requestData['id'];

            $list = $this->catalogManager->getPerticular($userId, $Id);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function integrate(Request $request): Response {
        try {
            $requestData = $this->requestData($request);
            $userId = $requestData['userId'];
            $prodyctID = $requestData['prodyctID'];
            $outputFormat = $requestData['outputformat'];
            if (!array_key_exists("erpid", $requestData)) {
                return $this->jsonResponse(null, "required  erpid", Response::HTTP_FORBIDDEN);
            }
            $erpid = $requestData['erpid'];
            if (empty($erpid)) {
                return $this->jsonResponse(null, "erpid can not be empty", Response::HTTP_FORBIDDEN);
            }
            $list = $this->catalogManager->integrate($userId, $prodyctID, $erpid);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
