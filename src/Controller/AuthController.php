<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DataTrans\Entities\WebEDI\Service\Oauth;
use App\Helper\Util;
//use Symfony\Component\Config\Definition\Exception\Exception;
use Exception;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
/**
 * Description of AuthController
 *
 * @author vikrant
 */
class AuthController extends AppController {

    private $partners;

    public function __construct(Oauth $oauth) {
        $this->oauth = $oauth;
    }

    public function login(Request $request): Response {
        // get the local secret key
          $secret = $this->getParameter('jwtSecretKey');
          $requestData = $this->requestData($request);
          $clietnSecret = $requestData['clietnSecret'];
          $clietnID = $requestData['clietnID'];

        if(empty($clietnSecret))
        {
          die('client secret can not be empty');
        }
        if(empty($clietnID))
        {
          die('client ID can not be empty');
        }


        $authresult = $this->oauth->getUserID($clietnID, $clietnSecret);
        if(empty($authresult))
        {
            die('client ID or client secret is not correct.');
        }
        $userId = $authresult['partnerId'];

        $payload = [
            'user_id' => $userId,
            'role' => 'admin',
            'exp' => 1593828222
        ];

        $token = Util::generateAuthToken($payload, $secret);
        $response = new Response();
        $response->setContent(json_encode([
            'success' => true,
            'token' => $token
        ]));
        $response->headers->set('x-auth-token', $token);
        return $response;
    }

}
