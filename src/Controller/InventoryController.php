<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Service\InventoryManager;

class InventoryController extends AppController {

    private $ShipmentManager;

    public function __construct(InventoryManager $inventorymanager) {
        $this->inventorymanager = $inventorymanager;
    }

    public function getAll(Request $request): Response {
        try {
            $requestData = $this->requestData($request);

            if (!array_key_exists("download", $requestData)) {
                return $this->jsonResponse(null, "required  download", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("startDate", $requestData)) {
                return $this->jsonResponse(null, "required  start date", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("endDate", $requestData)) {
                return $this->jsonResponse(null, "required  end date", Response::HTTP_FORBIDDEN);
            }

            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];
            $download = $requestData['download'];
            $sdate = $requestData['startDate'];
            $edate = $requestData['endDate'];
            $tp = '';
            $page = 1;
            $limit = 100;

            if (array_key_exists("tp", $requestData)) {
                $tp = $requestData['tp'];
            }

            if (array_key_exists("limit", $requestData) && !empty($requestData['limit'])) {
                $limit = $requestData['limit'];
            }
            if (array_key_exists("page", $requestData) && !empty($requestData['page'])) {
                $page = $requestData['page'];
            }

            if ($download == '') {
                return $this->jsonResponse(null, "download can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($sdate == '') {
                return $this->jsonResponse(null, "start date can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($edate == '') {
                return $this->jsonResponse(null, "end date can not be empty", Response::HTTP_FORBIDDEN);
            }

            if ($edate < $sdate) {
                return $this->jsonResponse(null, " start date can not be greater that end date", Response::HTTP_FORBIDDEN);
            }

            $list = $this->inventorymanager->getAll($userId, $download, $sdate, $edate, $tp, $limit, $page);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAllChangePO(Request $request): Response {
        try {

            $requestData = $this->requestData($request);

            if (!array_key_exists("download", $requestData)) {
                return $this->jsonResponse(null, "required  download", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("startDate", $requestData)) {
                return $this->jsonResponse(null, "required  start date", Response::HTTP_FORBIDDEN);
            }
            if (!array_key_exists("endDate", $requestData)) {
                return $this->jsonResponse(null, "required  end date", Response::HTTP_FORBIDDEN);
            }

            $userId = $requestData['userId'];
            $outputFormat = $requestData['outputformat'];
            $download = $requestData['download'];
            $sdate = $requestData['startDate'];
            $edate = $requestData['endDate'];
            $tp = '';
            
            if (array_key_exists("tp", $requestData)) {
                $tp = $requestData['tp'];
            }

            if ($download == '') {
                return $this->jsonResponse(null, "download can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($sdate == '') {
                return $this->jsonResponse(null, "start date can not be empty", Response::HTTP_FORBIDDEN);
            }
            if ($edate == '') {
                return $this->jsonResponse(null, "end date can not be empty", Response::HTTP_FORBIDDEN);
            }

            if ($edate < $sdate) {
                return $this->jsonResponse(null, "start date can not be greater that end date", Response::HTTP_FORBIDDEN);
            }

            $list = $this->inventorymanager->getAllChangePO($userId, $download, $sdate, $edate, $tp);
            return $this->response($list, $outputFormat);
        } catch (Exception $ex) {
            return $this->jsonResponse(null, $ex->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}

?>
