<?php
namespace App\EventListener;

use DataTrans\Entities\WebEDI\Service\Oauth;
use Symfony\Component\Config\Definition\Exception\Exception;

class KernelException
{
  public function __construct(Oauth $oauth)
  {
      $this->oauth    = $oauth;
  }

  public function onKernelRequest()
  {
        die('onKernelRequest');
  }
}


?>
