# This file is the entry point to configure your own services.
# Files in the packages/ subdirectory configure your dependencies.

# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
imports:
  - { resource: parameters.yaml }

parameters:

services:
  # default configuration for services in *this* file
  _defaults:
    autowire: true # Automatically injects dependencies in your services.
    autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.

  # makes classes in src/ available to be used as services
  # this creates a service per class whose id is the fully-qualified class name
  App\:
    resource: "../src/*"
    exclude:
      - "../src/DependencyInjection/"
      - "../src/Entity/"
      - "../src/Kernel.php"
      - "../src/Tests/"

  # add Event Listeners
  listener.request:
    class:
        App\Service\Security\AuthListener
    arguments: ["%jwtSecretKey%"]
    tags:
      - {
          name: kernel.event_listener,
          event: kernel.request,
          method: onKernelRequest,
        }

  app.RateListener:
      class: App\Service\Security\RateListener
      tags:
        - {
          name: kernel.event_listener,
          event: kernel.request,
          method: onKernelRequest,
          }

  # app.CustException:
  #     class: App\Service\Security\CustException
  #     tags:
  #       - {
  #         name: kernel.event_listener,
  #         event: kernel.request,
  #         method: onKernelException,
  #         }

  app.ExceptionListener:
      class: App\Service\Security\ExceptionListener
      tags:
        - {
          name: kernel.event_listener,
          event: kernel.exception,
          method: onKernelException,
          }

    #     App\Service\Security\RateListener
    # tags:
    #   - {
    #       name: kernel.event_listener,
    #       event: kernel.request,
    #       method: onKernelRequest,
    #     }

  # controllers are imported separately to make sure services can be injected
  # as action arguments even if you don't extend any base controller class
  App\Controller\:
    resource: "../src/Controller/"
    tags: ["controller.service_arguments"]

  # App\EventListener\KernelException:
  #   tags:
  #     - { name: kernel.event_listener ,event: kernel.exception}

  # add more service definitions when explicit configuration is needed
  # please note that last definitions always *replace* previous ones

  DataTrans\PDOService\PDOConnector: "@pdo_webedi"
  pdo_webedi:
    class: 'DataTrans\PDOService\PDOConnector'
    arguments:
      [
        "%webedi_db_type%",
        "%webedi_db_host%",
        "%webedi_db_port%",
        "%webedi_db_name%",
        "%webedi_db_user%",
        "%webedi_db_password%"
      ]

  DataTrans\Entities\WebEDI\Service\Partners: "@partners"
  partners:
    class: 'DataTrans\Entities\WebEDI\Service\Partners'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Catalogs: "@catalogs"
  catalogs:
    class: 'DataTrans\Entities\WebEDI\Service\Catalogs'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Oauth: "@oauth"
  oauth:
    class: 'DataTrans\Entities\WebEDI\Service\Oauth'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Orders: "@orders"
  orders:
    class: 'DataTrans\Entities\WebEDI\Service\Orders'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Invoices: "@invoices"
  invoices:
    class: 'DataTrans\Entities\WebEDI\Service\Invoices'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Documents: "@documents"
  documents:
    class: 'DataTrans\Entities\WebEDI\Service\Documents'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\PartnerConfigs: "@partnerConfigs"
  partnerConfigs:
    class: 'DataTrans\Entities\WebEDI\Service\PartnerConfigs'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\DataTree: "@dataTree"
  dataTree:
    class: 'DataTrans\Entities\WebEDI\Service\DataTree'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Nodes: "@nodes"
  nodes:
    class: 'DataTrans\Entities\WebEDI\Service\Nodes'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Elements: "@elements"
  elements:
    class: 'DataTrans\Entities\WebEDI\Service\Elements'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Validations: "@validations"
  validations:
    class: 'DataTrans\Entities\WebEDI\Service\Validations'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\PartnerAddresses: "@partnerAddresses"
  partnerAddresses:
    class: 'DataTrans\Entities\WebEDI\Service\PartnerAddresses'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Mapper: "@mapper"
  mapper:
    class: 'DataTrans\Entities\WebEDI\Service\Mapper'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Shipments: "@shipments"
  shipments:
    class: 'DataTrans\Entities\WebEDI\Service\Shipments'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Messages: "@messages"
  messages:
    class: 'DataTrans\Entities\WebEDI\Service\Messages'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\SmartFilters: "@smartFilters"
  smartFilters:
    class: 'DataTrans\Entities\WebEDI\Service\SmartFilters'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\FormDefaults: "@formDefaults"
  formDefaults:
    class: 'DataTrans\Entities\WebEDI\Service\FormDefaults'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\PartnerDefaults: "@partnerDefaults"
  partnerDefaults:
    class: 'DataTrans\Entities\WebEDI\Service\PartnerDefaults'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\Inventory: "@inventory"
  inventory:
    class: 'DataTrans\Entities\WebEDI\Service\Inventory'
    arguments: ["@pdo_webedi"]
    public: true

  DataTrans\Entities\WebEDI\Service\APIDocument: "@APIDocument"
  APIDocument:
    class: 'DataTrans\Entities\WebEDI\Service\APIDocument'
    arguments: ["@pdo_webedi"]
    public: true

  # listener.request:
  #     class: App\Service\Security\AuthListener
  #     arguments: ['@session_manager']
  #     tags:
  #         - { name: kernel.event_listener, event: kernel.request, method: onKernelRequest }
